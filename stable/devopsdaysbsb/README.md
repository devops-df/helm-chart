# DevOpsDays Brasília

O DevOpsDays Brasília, associado e reconhecido pelo DevOpsDays.org, tem como objetivo reunir os interessados no assunto DevOps através da apresentação de palestras, painéis, minicursos e outras atividades. Considerando o aumento do interesse no desenvolvimento de software e cultura Ágil no Brasil, acreditamos que falar sobre DevOps é uma necessidade.

O DevOpsDays Brasília preenche essa lacuna, uma vez que esse assunto ainda não foi tratado de forma sistemática em um evento próprio, mesmo com a atual e crescente demanda no Brasil. Pretendemos para esse evento convidar profissionais de empresas e organizações conhecidas no cenário de tecnologia da informação, para assim motivar a ampla participação, não somente de pessoas que já conhecem a cultura DevOps, mas também despertar o interesse daqueles que ainda não conhecem.

## Hot site

Este template implementa o *hot site* do evento ocorrido em 2017.

*Keep* ***C.A.L.M.S*** *and have fun!*
